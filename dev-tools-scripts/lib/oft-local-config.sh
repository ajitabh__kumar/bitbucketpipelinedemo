#!/bin/bash
#<%%#
####################################################################################################
#
# FILENAME:     local-config-template.sh.ejs
#
# PURPOSE:      Template for creating a personalized local-config.sh file.
#
# DESCRIPTION:  All shell scripts in the dev-tools directory require several configuration values
#               to run correctly (eg. the path to your project's root directory or the alias of
#               the DevHub that you want to use.  These customizations can be specific to each
#               individual developer, and therefore should not be tracked by the project's VCS.
#
#               This file serves as a template that Release Managers can use to establish baseline
#               values for their project.  When individaul developers clone the repo and initialize
#               their local, personal version of the project this template is used to create a
#               customized local-config.sh file at <project-root>/dev-tools/lib/local-config.sh
#
# INSTRUCTIONS: 1. Inside of the dev-tools directory, execute the following command
#                  cp ./templates/local-config-template.sh.ejs ./lib/local-config.sh
#               2. Edit the default values in your local-config.sh to meet the needs of your local
#                  environment and project setup.
#
####################################################################################################
#%>
##
###
#### DEFINE LOCAL CONFIGURATION VARIABLES ##########################################################
###
##
#
# Alias for the Dev Hub that should be used when creating scratch orgs for this project.
# This variable will always need to be customized for individual developers.
DEV_HUB_ALIAS="proj-devhub"

# Namespace Prefix.  Set to empty string ("") if this project is not building a managed package.
NAMESPACE_PREFIX=""
PSEUDO_NAMESPACE_PREFIX="proj"

# Package Name.  Specified as part of the Package Detail info in your packaging org. 
# Surround this value with double-quotes if your package name contains space characters.
# Set to empty string ("") if this project is not building a managed package.
PACKAGE_NAME=""

# Metadata Package ID.  Refers to the metadata package as a whole.  Must begin with "033".
# Set to empty string ("") if this project is not building a managed package.
METADATA_PACKAGE_ID=""

# Package Version ID. Refers to a specific, installable version of a package. Must begin with "04t".
# Set to empty string ("") if this project is not building a managed package.
PACKAGE_VERSION_ID=""

# Default Package Directory. Should match what is set in sfdx-project.json.
DEFAULT_PACKAGE_DIR_NAME="force-app"

# Alias for the primary Scratch Org used by this project.
#SCRATCH_ORG_ALIAS="$NAMESPACE_PREFIX-SCRATCH"

# Alias for the packaging org for this project.
#PACKAGING_ORG_ALIAS="$NAMESPACE_PREFIX-PACKAGE"

# Alias for the subscriber test org used to test managed-beta package installs.
#SUBSCRIBER_ORG_ALIAS="$NAMESPACE_PREFIX-SUBSCRIBER"

# Git Remote URI. SSH or HTTPS URI that points to the Git remote repo used by this project.
# GitHub is used as an example here, but any Git remote (ie. BitBucket) can be used.
# Set to empty string ("") if this project is not being tracked in a remote repository.
#GIT_REMOTE_URI="<%-userAnswers.gitRemoteUri%>"

# Location of the primary scratch-def.json file that should be used by SFDX-Falcon scripts that
# create scratch orgs (eg. rebuild-scratch-org).
#SCRATCH_ORG_CONFIG="$PROJECT_ROOT/config/project-scratch-def.json"
SCRATCH_ORG_CONFIG="config/project-scratch-def.json"

# Location of the primary project-user-def.json file that could be used
PROJECT_USER_CONFIG="config/project-user-def.json"
PROJECT_USER_DISPLAY_DIR="config/"

## Or for scalability to bigger teams. 
## Use Associative arrays aids in alignment with unique SF username requirements.
declare -A PROJECT_USERS
declare -A PROJECT_USER

### Define a user, noting any unique aspects should be defined as array elements
PROJECT_USER["definition_file"]="config/project-user-def.json"  # common or discrete definition, doesn
PROJECT_USER["empty_attribute"]="future-scope"                  # spare for future scope
project_user_string=$(declare -p PROJECT_USER)
PROJECT_USERS["qa-user1"]=${project_user_string}  # < add with unique Username prefix

### Define next user, noting any unique aspects should be defined as array elements
#PROJECT_USER["definition_file"]="config/project-user-def.json"  # common or discrete definition, doesn
#PROJECT_USER["empty_attribute"]="future-scope2"                 # spare for future scope
#project_user_string=$(declare -p PROJECT_USER)
#PROJECT_USERS["qa-user2"]=${project_user_string}  # < add with unique Username prefix


# Data Plan when initing scratch org
# TODO make array based
DATA_PLAN="data/data-plan.json"
DATA_PLAN_SUPPLIERS="data/data-plan-suppliers.json"
DATA_PLAN_POLICYBATCH_VOLVO="data/data-plan-policyBatch-Volvo_20200828_1613.json"


# Get the current git branch if applicable
GIT_CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
## Remove / , ie; "feature/f1" becomes "feature-f1" 
git_branch_replacement="-"
GIT_CURRENT_BRANCH_FOLDER_SAFE=${GIT_CURRENT_BRANCH//\//$git_branch_replacement}


# Echo the variables set by this script prior to exiting.  Specify "false" to suppress the
# display of local config that normally occurs when executing SFDX-Falcon based scripts.
ECHO_LOCAL_CONFIG_VARS="true"

#
##
###
#### ECHO ALL VARIABLES ############################################################################
###
##
#
if [ "$ECHO_LOCAL_CONFIG_VARS" = "true" ]; then
  echo "\n`tput setaf 7``tput bold`Local configuration variables set by `dirname $0`/lib/local-config.sh`tput sgr0`\n"
fi
##END##