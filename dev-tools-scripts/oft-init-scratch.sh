## Deactivate Duplicate Rules
#!/bin/bash
####################################################################################################
#
### Script Name: dev-tools-scripts/oft-init-scratch.sh
### Author: Paul Carmuciano 2018-12-06

### Description: Used to spin up a new Scratch org from Source (prob could convert to package)
###     Also handles test build

### Run Information: This script is run manually, requires params ie;
###     .\.scripts\oft-init-scratch.sh --devhubalias "sqp-dx1-devHub" --scratchorgalias "oftdx-mp-init1" --testonly

# @mod-log 
# -----------------------------------------------------------------------------------------------------
# Developer         Date            Mod ID      Description
# -----------------------------------------------------------------------------------------------------
# Paul              2018-12-06      1000        Initial version
# Yi                2021-04-27      2000        Removed project specific scripts, make it boilerplate
#
####################################################################################################

#set -x

#
##
###
#### LOAD SHARED FUNCTIONS LIBRARY #################################################################
###
##
#
#if [ ! -r `dirname $0`/lib/shared-functions.sh ]; then
#  echo "\nFATAL ERROR: `tput sgr0``tput setaf 1`Could not load dev-tools/lib/shared-functions.sh.  File not found.\n"
#  exit 1
#fi
#source `dirname $0`/lib/shared-functions.sh

# Just vars for now
if [ ! -r `dirname $0`/lib/oft-local-config.sh ]; then
  echo "\nFATAL ERROR: `tput sgr0``tput setaf 1`Could not load ./lib/oft-local-config.sh.  File not found.\n"
  exit 1
fi
source `dirname $0`/lib/oft-local-config.sh


#
##
###
#### FUNCTION: checkParamOrSet ##############################################################
###
##
#
checkParamOrSet () {
  return_var=""
  if [[ $1 = "" ]]; then
    return_var=$2
  else
    return_var=$1
  fi
  echo $return_var
}



#
##
###
#### CONFIRM SCRIPT EXECUTION ######################################################################
###
##
#
#confirmScriptExecution "##ADD_CONFIRMATION_QUESTION##?"
#
##
###
#### CREATE LOCAL VARIABLES ########################################################################
###
##
#

# get params
while [[ "$#" -gt 0 ]]
  do
    case $1 in
        -d|--devhubalias)
        prm_dev_hub_alias="$2"
    ;;
        -s|--scratchorgalias)
        prm_scratch_org_alias="$2"
    ;;
        --context-cicd)
        prm_context_cicd=true
    ;;
        --skipusers)
        prm_skip_users=true
    ;;
        -t|--testonly)
        prm_test_only=true
    ;;
    esac    
  shift
done

# Check params / define locals
## Find branch, ie to make feature scratch org
echo "GIT_CURRENT_BRANCH_FOLDER_SAFE $GIT_CURRENT_BRANCH_FOLDER_SAFE"

## Set the Dev Hub Alias
dev_hub_alias=$(checkParamOrSet "$prm_dev_hub_alias" "$DEV_HUB_ALIAS")
echo "dev_hub_alias $dev_hub_alias"

## Set the scratch org alias
scratch_org_alias=$(checkParamOrSet "$prm_scratch_org_alias" "$PSEUDO_NAMESPACE_PREFIX-$GIT_CURRENT_BRANCH_FOLDER_SAFE")
echo "scratch_org_alias $scratch_org_alias"

# Check params / define locals

## check for key params
if [[ $dev_hub_alias = "" ]] ||
  [[ $scratch_org_alias = "" ]] ; then
    echo
    echo 'Missing arguments'
    read -rsp $'Press enter to exit...\n'
    exit 3
fi

#
##
###
#### CUSTOM CODE BEGINS HERE #######################################################################
###
##
#

sfdx plugins --core

# Execute
## Create the org
echo ""
echo \
  "sfdx force:org:create \\
  --definitionfile $SCRATCH_ORG_CONFIG \\
  --targetdevhubusername $dev_hub_alias \\
  --setalias $scratch_org_alias \\
  --apiversion 49.0 \\
  orgName=$scratch_org_alias\\
  --setdefaultusername \\
  --durationdays 30 \\
  \n"
(exec sfdx force:org:create \
  --definitionfile $SCRATCH_ORG_CONFIG \
  --targetdevhubusername "$dev_hub_alias" \
  --setalias "$scratch_org_alias" \
  --apiversion 49.0 \
  orgName="$scratch_org_alias" \
  --setdefaultusername \
  --durationdays 30)
# Check if the previous command executed successfully. If not, abort this script.
if [ $? -ne 0 ]; then
  echo "Command failed. Terminating script."
  exit 1
fi


#
##
### Pre-deploy any other Metadata items that are untracked, prior to push to avoid losing force:status
##
#

#
##
### Now Push source (across tracked locations)
##
#

## Push source (across tracked locations)
echo ""
echo \
  "sfdx force:source:push -f \\
  -u $scratch_org_alias \\
  \n"
(exec sfdx force:source:push -f \
  -u $scratch_org_alias)
# Check if the previous command executed successfully. If not, abort this script.
if ([ $? -ne 0 ] \
 && [ "$prm_context_cicd" = true ] \
); then
  sfdx force:org:delete -p -u "$scratch_org_alias"
  echo "Command failed. Terminating script."
  exit 1
fi

## Create Additional Users

## handle testonly outcome
if [[ "$prm_test_only" = true ]] ; then
    sfdx force:org:delete -u $scratch_org_alias --noprompt
    echo
    echo 'Source test passed.'
else
    echo
    echo "Scratch Org $scratch_org_alias is set up"
    #sfdx force:org:open -u $scratch_org_alias
fi

#
##
###
#### ECHO CLOSING MESSAGE ##########################################################################
###
##
#
echo
read -rsp $'\nScript completed...\n'
exit 0


















#
##
###
#### FUNCTION: showPressAnyKeyPrompt ###############################################################
###
##
#
function myfunc() {
    local  __resultvar=$1
    local  myresult='some value'
    if [[ "$__resultvar" ]]; then
        eval $__resultvar="'$myresult'"
    else
        echo "$myresult"
    fi
}

myfunc result
echo $result
result2=$(myfunc)
echo $result2
